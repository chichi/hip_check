CMake HIP check
---------------

Minimal working example of using HIP on raven with CMake.

**How to**:

    * clone repository on raven
    * execute `bash test.sh`

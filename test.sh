module purge
module load cmake
module load gcc/12
module load cuda/12.2
module load hip/5.7.1

export CC=gcc
export CXX=g++
export ROCM_PATH=${HIP_HOME}

rm -rf build
cmake -S . -B build


#!/bin/bash
#SBATCH --job-name=hip_job                  # Job name
#SBATCH --output=hip_job_output.txt         # Output file
#SBATCH --error=hip_job_error.txt           # Error file
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:15:00
#SBATCH --mem=64G
#SBATCH --partition=gpu-dev
#SBATCH --gres=gpu:a100:1
#SBATCH --cpus-per-task=1

module purge
module load cmake
module load gcc/12
module load cuda/12.2
module load hip/5.7.1

export CC=gcc
export CXX=hipcc

rm -rf build
cmake -S . -B build
cd build
make
./hip_hello_world_cuda
